//
//  NbaTeam.swift
//  Rss Reader
//
//  Created by Marko Piskac on 05/12/2017.
//  Copyright © 2017 Marko Piskac. All rights reserved.
//

import RealmSwift

class NbaTeam: Object {
    let id = RealmOptional<Int>()
    @objc dynamic var teamName : String?
    @objc dynamic var rssLink : String?
    @objc dynamic var imageName : String?
    @objc dynamic var numberOfNewPosts : Int = 0
    let followed = RealmOptional<Bool>()
    var newsFeed = List<NewsFeed>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
