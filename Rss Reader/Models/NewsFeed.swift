//
//  File.swift
//  Rss Reader
//
//  Created by Marko Piskac on 05/12/2017.
//  Copyright © 2017 Marko Piskac. All rights reserved.
//

import RealmSwift

class NewsFeed: Object {
    @objc dynamic var pubDate : Date?
    @objc dynamic var title : String?
    @objc dynamic var feedDescription : String?
    @objc dynamic var linkUrl : String?
    @objc dynamic var imageUrl : String? = ""

}
