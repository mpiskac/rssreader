//
//  UserdFeed.swift
//  Rss Reader
//
//  Created by Marko Piskac on 09/12/2017.
//  Copyright © 2017 Marko Piskac. All rights reserved.
//
import RealmSwift

class UserFeed: Object {
    @objc dynamic var id : String?
    @objc dynamic var feedTitle : String?
    @objc dynamic var imageUrl : String?
    @objc dynamic var rssLink : String?
    @objc dynamic var numberOfNewPosts : Int = 0
    var newsFeed = List<NewsFeed>()
    override static func primaryKey() -> String? {
        return "id"
    }
}
