//
//  AppDelegate.swift
//  Rss Reader
//
//  Created by Marko Piskac on 5/12/2017.
//  Copyright © 2017 Marko Piskac. All rights reserved.
//

import UIKit
import RealmSwift
import Fabric
import Crashlytics
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var teamNotificationToken :NotificationToken?
    var userFeedNotificationToken: NotificationToken?
    var timer: Timer?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        Fabric.with([Crashlytics.self])
        setDatabase()
        setRealmNotification()
        let notificationSettings = UIUserNotificationSettings(types: [.alert,.sound], categories: nil)
        UIApplication.shared.registerUserNotificationSettings(notificationSettings)
        setTimer()
        
        return true
    }
    
    func setDatabase() {
        let realm = try! Realm()
        if realm.objects(NbaTeam.self).count > 0 {
            refreshNewsFeed()
        } else {
            if let path = Bundle.main.path(forResource: "nbaTeams", ofType: "json") {
                do {
                    let rawData = try NSData(contentsOf: NSURL(fileURLWithPath: path) as URL, options: NSData.ReadingOptions.mappedIfSafe)
                    ParseNbaTeams(jsondata: rawData, completion: { (success) in
                        if success {
                            self.refreshNewsFeed()
                        }
                    })
                } catch {
                    print("Something went wrong with fetching data")
                }
            }
            

        }
        self.refreshUserStoryFeed()
    }
     func refreshNewsFeed() {
        ParseNewsFeed(teamId: nil, completion: { (success) in
            if success {
                print("succesfully fetched nba storys")
            }
        })
    }
     func refreshUserStoryFeed() {
        ParseUsersFeed(feedId: nil, completion: { (success) in
            if success {
                print("succesfully fetched user storys")
            }
        })
    }
    func setTimer() {
        timer = Timer()
        timer = Timer.scheduledTimer(timeInterval: 300,
                                     target: self,
                                     selector: #selector(refreshNews),
                                     userInfo: nil,
                                     repeats: true)
        
    }
    @objc func refreshNews() {
        refreshNewsFeed()
        refreshUserStoryFeed()
    }
    func setRealmNotification(){
        let realm = try! Realm()
        teamNotificationToken = realm.objects(NewsFeed.self).observe({ (changes) in
            switch changes {
                
            case .initial(_):
                print("notification token initialized")
            case .update(_, let deletions, let insertions, let modifications):
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: Notification.Name("User News Fetched"), object: nil)
                    NotificationCenter.default.post(name: Notification.Name("News Fetched"), object: nil)
                }
                if insertions.count > 0 {
                    self.createLocalNotification()

                }
                if insertions.count > 0 && UIApplication.shared.applicationState == .background {
                    self.createLocalNotification()
                }
            case .error(_):
                print("error in notification token")
            }
        })
   
    }

     func createLocalNotification() {
        let localNotification = UILocalNotification()
        localNotification.fireDate = Date(timeIntervalSinceNow: 5)
        //localNotification.applicationIconBadgeNumber = 1
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.userInfo = [
            "message" : "You got new storys "
        ]
        localNotification.alertBody = "Check them out"
        UIApplication.shared.scheduleLocalNotification(localNotification)
    }
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        takeActionForLocalNotification(notification)
        
    }
    func takeActionForLocalNotification(_ localNotification: UILocalNotification) {
        NotificationCenter.default.post(name: Notification.Name("Scroll To News"), object: nil)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        timer?.invalidate()
        teamNotificationToken?.invalidate()
    }


}

