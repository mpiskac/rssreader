//
//  WebViewVC.swift
//  Rss Reader
//
//  Created by Marko Piskac on 05/12/2017.
//  Copyright © 2017 Marko Piskac. All rights reserved.
//

import UIKit

class WebViewVC: UIViewController, UIWebViewDelegate {
    
    var feedUrl:String?
    var overlay:UIView?
    var activityIndicator: UIActivityIndicatorView?
    var first = true
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        overlay = UIView(frame: view.frame)
        overlay?.backgroundColor = UIColor.black
        overlay?.alpha = 0.8
        
        self.navigationItem.title = "Web View"
        self.navigationController?.navigationBar.topItem?.title = " "


        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicator?.center = self.view.center

        webView.delegate = self
        if let url = URL(string: feedUrl!) {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
            self.tabBarController?.tabBar.isHidden = true
    }
    func webViewDidStartLoad(_ webView: UIWebView) {
        if first {
            first = false
            self.view.addSubview(overlay!)
            self.overlay?.addSubview(activityIndicator!)
            activityIndicator?.startAnimating()
        }
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if activityIndicator != nil {
            activityIndicator?.stopAnimating()
            activityIndicator?.removeFromSuperview()
        }
        if overlay != nil {
            overlay?.removeFromSuperview()
        }
        
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        activityIndicator?.stopAnimating()
        activityIndicator?.removeFromSuperview()
        overlay?.removeFromSuperview()
        self.dismiss(animated: true, completion: nil)
    }
}


    


