//
//  UserFeedsVC.swift
//  Rss Reader
//
//  Created by Marko Piskac on 09/12/2017.
//  Copyright © 2017 Marko Piskac. All rights reserved.
//

import UIKit
import RealmSwift

protocol RefreshUserFeeds {
   func  refreshFeeds()
}

class UserFeedsVC: UITableViewController,RefreshUserFeeds {
    
    var txtField1: UITextField!
    var txtField2: UITextField!
    var feeds: Results<UserFeed>? = nil
    var selectedStoryId: String?
    var selectedStoryName: String?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]

        navigationController?.navigationBar.tintColor = .white
        let add = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        navigationItem.rightBarButtonItems = [add]


        
        let realm = try! Realm()
        feeds = realm.objects(UserFeed.self)
        
        tableView.register(TeamCell.self, forCellReuseIdentifier: "TeamCell")
        self.tableView.tableFooterView = UIView()
        
         NotificationCenter.default.addObserver(self, selector: #selector(refreshFeeds), name: Notification.Name("User News Fetched"), object: nil)
    }
    deinit {
        NotificationCenter.default.removeObserver("User News Fetched")

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.topItem?.title = "Your RSS Reader "
        self.tabBarController?.tabBar.isHidden = false
    }
    @objc func refreshFeeds() {
        self.tableView.reloadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds?.count ?? 0
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell: TeamCell = tableView.dequeueReusableCell(withIdentifier: "TeamCell", for: indexPath) as! TeamCell
        guard let feed = feeds?[indexPath.row] else {return cell}
        cell.teamName.text = feed.feedTitle
        if feed.imageUrl != nil  {
            cell.teamLogo.sd_setImage(with: URL(string: feed.imageUrl!), placeholderImage: UIImage(named: "rssImg"))
        } else {
            cell.teamLogo.image = UIImage(named:"rssImg")
        }
        if feed.numberOfNewPosts > 0 {
            cell.badgeLabel.alpha = 1.0
            cell.badge.alpha = 1.0
            cell.badgeLabel.text = "\(feed.numberOfNewPosts)"
        } else {
            cell.badgeLabel.alpha = 0
            cell.badge.alpha = 0
        }
        cell.followButton.alpha = 0
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let story = feeds?[indexPath.row] else {return}
        selectedStoryId = story.id
        selectedStoryName = story.feedTitle
        performSegue(withIdentifier: "showUserNews", sender: self)
    }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard let feed = feeds?[indexPath.row] else {return }

            RemoveUserFeed(rssLink: feed.id!, completion: { (succes) in
                if succes == true {
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            })
        }
        
    }
   
    @objc func addTapped() {
        let alert = UIAlertController(title: "Add new feed to your collection", message: "Enter rss url and the title", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addTextField(configurationHandler: addTextField1)
        alert.addTextField(configurationHandler: addTextField2)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: { (UIAlertAction)in
            print("Cancel")
        }))
        alert.addAction(UIAlertAction(title: "Add", style: UIAlertActionStyle.default, handler:{ (UIAlertAction)in
            if (self.txtField1.text?.characters.count)! > 0 && (self.txtField2.text?.characters.count)! > 0 {
                AddNewUserFeed(rssLink: self.txtField1.text!, title: self.txtField2.text!, completion: {success in
                    if success {
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                        ParseUsersFeed(feedId: self.txtField1.text, completion: { (success) in
                            if success == true {
                               print("storys fetched")
                            } else {
                                self.errorFetchingRss()
                            }
                        })
                    } else {
                        self.errorFetchingRss()
                    }
                   
                })
            }
            print("Add")
            print("rss url : \(self.txtField1.text!)")
            print("title : \(self.txtField2.text!)")
        }))
        self.present(alert, animated: true, completion: nil)
    }
    func errorFetchingRss() {
        let alert = UIAlertController(title:  "Something went wrong", message: "Check you connection and rss link", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3, execute: {
            alert.dismiss(animated: true, completion: nil)
        })
    }
    func addTextField1(textField: UITextField!)
    {
        textField.placeholder = "rss url"
        txtField1 = textField
    }
    
    func addTextField2(textField: UITextField!)
    {
        textField.placeholder = "title"
        txtField2 = textField
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: StoryNewsVC.self) {
            let destinationVc = segue.destination as? StoryNewsVC
            destinationVc?.storyId = selectedStoryId
            destinationVc?.delegateUserFeed = self
            destinationVc?.navTitle = selectedStoryName
            
        }
    }
}
