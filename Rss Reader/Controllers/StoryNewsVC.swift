//
//  TeamNewsVC.swift
//  Rss Reader
//
//  Created by Marko Piskac on 05/12/2017.
//  Copyright © 2017 Marko Piskac. All rights reserved.
//

import UIKit
import RealmSwift
import SDWebImage



class StoryNewsVC: UITableViewController {
    var teamId: Int?
    var storyId: String?
    var team: NbaTeam?
    var storyFeed: UserFeed?
    var feedUrl: String?
    var delegateUserFeed: RefreshUserFeeds?
    var delegateTeams: RefreshTeamsFeeds?
    var navTitle:String?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.topItem?.title = " "
//        let clear = UIBarButtonItem(title: "Clear", style: .plain, target: self, action: #selector(clearTapped))
//        navigationItem.rightBarButtonItems = [clear]
        
        
        let realm = try! Realm()
        if teamId != nil {
            team =  realm.objects(NbaTeam.self).filter("id = \(teamId ?? 0)").first
            if team == nil {
                self.dismiss(animated: true, completion: nil)
            } else if team!.numberOfNewPosts > 0 {
                
                resetNotificationsNUmber(teamId: teamId, storyId: nil, completion: {_ in
                })
            }
        } else {
            let qStr = " "
            storyFeed =  realm.objects(UserFeed.self).filter("id = '\(storyId ?? qStr)'").first
            if storyFeed == nil {
                self.dismiss(animated: true, completion: nil)
            } else if storyFeed!.numberOfNewPosts > 0 {
                resetNotificationsNUmber(teamId: nil, storyId: storyId, completion: {_ in
                })
            }
        }
       
        tableView.tableFooterView = UIView()

        let refreshControl = UIRefreshControl()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        if team?.newsFeed != nil && team?.newsFeed.count == 0 {
         showRefreshAlert()
        } else if storyFeed?.newsFeed != nil && storyFeed?.newsFeed.count == 0 {
            showRefreshAlert()
        }
    }
//   @objc func clearTapped() {
//        if team != nil {
//            RemoveStorysForFeed(teamId: nil, storyId: storyId, completion: { success in
//                if success == true {
//                    self.tableView.reloadData()
//                }
//            })
//        } else {
//            RemoveStorysForFeed(teamId: teamId, storyId: nil, completion: { success in
//                if success == true {
//                    self.tableView.reloadData()
//                }
//            })
//        }
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationItem.title = navTitle
        self.tabBarController?.tabBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(true)
        if delegateUserFeed != nil {
            delegateUserFeed?.refreshFeeds()
        } else if delegateTeams != nil {
            delegateTeams?.teamsUpdated()
        }
    }
    
    func showRefreshAlert() {
        let alert = UIAlertController(title: "Refresh news", message: "Just pull down to refresh", preferredStyle: .alert)
        self.present(alert, animated: true)
        let timeForAlertElapsed = DispatchTime.now() + 3
        DispatchQueue.main.asyncAfter(deadline: timeForAlertElapsed, execute: {
            alert.dismiss(animated: true, completion: nil)
        })
    }
    
    @objc private func refreshData(_ sender: UIRefreshControl) {
        if team != nil {
            ParseNewsFeed(teamId: team?.id.value) { success in
                self.refreshControl?.endRefreshing()
                if success == false {
                    self.showErrorAlert()
                } else {
                    self.tableView.reloadData()
                }
                
            }
        } else {
            ParseUsersFeed(feedId: storyFeed?.id) { success in
                self.refreshControl?.endRefreshing()
                if success == false {
                    self.showErrorAlert()
                } else {
                    self.tableView.reloadData()
                }
                
            }
        }
    }
    func showErrorAlert(){
        var title: String?
        var message: String?
        if  Reachability.isConnectedToNetwork() {
            title = "App is not working correctly"
            message = "Sorry, try again later."
        } else {
            print("NO NETWORK")
            title = "No Internet Connection"
            message = "Make sure your device is connected to the internet."
        }
        let alert = UIAlertController(title:  title, message: message, preferredStyle: .alert)
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
            self.present(alert, animated: true, completion: {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3, execute: {
                    alert.dismiss(animated: true, completion: nil)
                })
            })
            
        })
    }
  
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if team != nil {
            return team?.newsFeed.count ?? 0
        } else {
            return (storyFeed?.newsFeed.count)!
        }
    }
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamNewsCell", for: indexPath) as! NewsCell
        var feed: AnyObject?
        if team != nil {
            let index = (team?.newsFeed.count)! - indexPath.row - 1
            feed = team?.newsFeed[index]
        } else {
            let index = (storyFeed?.newsFeed.count)! - indexPath.row - 1
            feed = storyFeed?.newsFeed[index]
        }
        if feed == nil {
            return cell
        }
        cell.titleLabel.text = feed?.title
        cell.descriptionLabel.text = feed?.feedDescription
        cell.pubDateLabel.text = feed?.pubDate??.UTCToLocal()
        if feed?.imageUrl != nil {
            cell.feedImage.sd_setImage(with: URL(string: (feed?.imageUrl)!), placeholderImage: UIImage(named: "rssImg"))
            }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var url: String?
        if team != nil {
            url = team?.newsFeed[indexPath.row].linkUrl
        } else {
            url = storyFeed?.newsFeed[indexPath.row].linkUrl
        }
        if url == nil {
            return
        }
        feedUrl = url
        performSegue(withIdentifier: "showWebView", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: WebViewVC.self) {
            let destinationVc = segue.destination as? WebViewVC
            destinationVc?.feedUrl = feedUrl
        }
    }
}
extension Date {
    func UTCToLocal() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm dd.MM.yyyy"
        dateFormatter.timeZone = NSTimeZone.local
        let localTimeZoneDateString = dateFormatter.string(from: self)
        return localTimeZoneDateString
    }
}
