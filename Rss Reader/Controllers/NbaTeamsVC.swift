//
//  ViewController.swift
//  Rss Reader
//
//  Created by Marko Piskac on 5/12/2017.
//  Copyright © 2017 Marko Piskac. All rights reserved.
//

import UIKit
import FeedKit
import RealmSwift

protocol RefreshTeamsFeeds {
    func  teamsUpdated()
}

class NbaTeamsVC: UIViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource,RefreshTeamsFeeds {
    
    var firstTime = true
    var selectedTeamId: Int?
    var selectedTeamName: String?
    var teams: Results<NbaTeam>? = nil
    var followedTeams: Results<NbaTeam>? = nil
    var currentPage = 1
    var shouldChangePage = true
    
    let headerView: UIView = {
       let view = UIView()
        view.backgroundColor = UIColor(red: 70/255, green: 150/255, blue: 241/255, alpha:0.8)
       view.translatesAutoresizingMaskIntoConstraints = false
       return view
    }()
    let newsLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = UIColor.clear
        let attributedText = NSAttributedString(string: "Team news", attributes: [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 18), NSAttributedStringKey.foregroundColor: UIColor.white])
        label.attributedText = attributedText
        label.textAlignment = .center
        return label
    }()
    let teamsLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.translatesAutoresizingMaskIntoConstraints = false
        label.backgroundColor = UIColor.clear
        let attributedText = NSAttributedString(string: "Teams", attributes: [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 18), NSAttributedStringKey.foregroundColor: UIColor.white])
        label.attributedText = attributedText
        
        label.textAlignment = .center
        return label
    }()
    let indicatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.yellow
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        return scrollView
    }()
   
    let newsTableView: UITableView = {
       let tableView = UITableView()
        //tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLineEtched
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
       return tableView
    }()
    let teamsTableView: UITableView = {
        let tableView = UITableView.init(frame: CGRect.zero, style: .grouped)
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .singleLineEtched
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0)
        //tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = false
        self.automaticallyAdjustsScrollViewInsets = false
        if firstTime{
            addSubviews()
        }

        let realm = try! Realm()
        teams = realm.objects(NbaTeam.self).filter("followed = false")
        followedTeams = realm.objects(NbaTeam.self).filter("followed = true")
        
        scrollView.delegate = self
        newsTableView.register(TeamCell.self, forCellReuseIdentifier: "TeamCell")
        newsTableView.delegate = self
        newsTableView.dataSource = self
        
        teamsTableView.register(TeamCell.self, forCellReuseIdentifier: "TeamCell")
        teamsTableView.delegate = self
        teamsTableView.dataSource = self
        
        
        let tapNews = UITapGestureRecognizer(target: self, action: #selector(newsLabelTapped) )
        newsLabel.addGestureRecognizer(tapNews)
        let tapTeams = UITapGestureRecognizer(target: self, action: #selector(teamsLabelTapped) )
        teamsLabel.addGestureRecognizer(tapTeams)
        
        NotificationCenter.default.addObserver(self, selector: #selector(teamsUpdated), name: Notification.Name("News Fetched"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(scrollToNews), name: Notification.Name("Scroll To News"), object: nil)

    
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.topItem?.title = "NBA Reader "
        self.tabBarController?.tabBar.isHidden = false
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if firstTime {
            setupLayout()
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setTableViewsLayout()
        if firstTime {
            self.newsTableView.reloadData()
            self.teamsTableView.reloadData()
        }
        shouldChangePage = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        firstTime = false
    }
    
        
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            self.indicatorView.alpha = 0
            let orient = UIApplication.shared.statusBarOrientation
            switch orient {
            case .portrait:
                
                print("Portrait")
            case .landscapeLeft,.landscapeRight :
                
                print("Landscape")
            default:
                print("default")
            }
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            if self.currentPage == 1 {
                self.scrollView.setContentOffset(CGPoint(x:0,y:0), animated: false)
                self.indicatorView.frame.origin.x = 0
            } else {
               
                self.indicatorView.frame.origin.x = self.view.center.x
                self.scrollView.setContentOffset(CGPoint(x:self.scrollView.frame.width,y:0), animated: false)
            }
            self.indicatorView.alpha = 1
        })
        super.viewWillTransition(to: size, with: coordinator)
        shouldChangePage = false
    }

    deinit {
        NotificationCenter.default.removeObserver("News Fetched")
        NotificationCenter.default.removeObserver("Scroll To News")

    }

    
    @objc func teamsUpdated() {
        DispatchQueue.main.async {
            self.newsTableView.reloadData()
            self.teamsTableView.reloadData()
        }
    }
    @objc func scrollToNews() {
        if scrollView.contentOffset.x > 0 {
            let scrollRect : CGRect = CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: self.scrollView.frame.size.height)
            currentPage = 1
            self.scrollView.scrollRectToVisible(scrollRect, animated: true)
            
        }
    }

    
    func addSubviews() {

        self.view.addSubview(headerView)
        headerView.addSubview(newsLabel)
        headerView.addSubview(teamsLabel)
        headerView.addSubview(indicatorView)
        view.addSubview(scrollView)
        scrollView.addSubview(teamsTableView)
        scrollView.addSubview(newsTableView)
    }
    
    func setupLayout() {
       
        //headerView.topAnchor.constraint(equalTo: (navigationController?.navigationBar.bottomAnchor)!).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        if #available(iOS 11.0, *) {
            headerView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
            headerView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
            headerView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        } else {
            headerView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
            headerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
            headerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        }
        
        newsLabel.topAnchor.constraint(equalTo: self.headerView.topAnchor).isActive = true
        newsLabel.leadingAnchor.constraint(equalTo: self.headerView.leadingAnchor).isActive = true
        newsLabel.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 1.0).isActive = true
        newsLabel.widthAnchor.constraint(equalTo: self.headerView.widthAnchor, multiplier: 0.5).isActive = true
        teamsLabel.topAnchor.constraint(equalTo: self.headerView.topAnchor).isActive = true
        teamsLabel.trailingAnchor.constraint(equalTo: self.headerView.trailingAnchor).isActive = true
        teamsLabel.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 1.0).isActive = true
        teamsLabel.widthAnchor.constraint(equalTo: self.headerView.widthAnchor, multiplier: 0.5).isActive = true
        
        indicatorView.bottomAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
//         indicatorView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor).isActive = true
        indicatorView.widthAnchor.constraint(equalTo: self.headerView.widthAnchor, multiplier: 0.5).isActive = true
        indicatorView.heightAnchor.constraint(equalTo: headerView.heightAnchor, multiplier: 0.1).isActive = true

        scrollView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        if #available(iOS 11.0, *) {
            scrollView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
            scrollView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
            scrollView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            scrollView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor,constant: 100).isActive = true
            scrollView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
            scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        }

    
//        newsTableView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
//        newsTableView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
//        newsTableView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
//        newsTableView.widthAnchor.constraint(equalToConstant: self.view.frame.width)
        
//        newsTableView.bounces = false
//        newsTableView.alwaysBounceVertical = false
//        teamsTableView.bounces = false
//        teamsTableView.alwaysBounceVertical = false
//        teamsTableView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
//        teamsTableView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
//        teamsTableView.leadingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
//        teamsTableView.widthAnchor.constraint(equalToConstant: scrollView.frame.width).isActive = true

       }
    
    func setTableViewsLayout() {
        scrollView.contentSize = CGSize(width: self.scrollView.frame.width * 2, height: 1)
        newsTableView.frame = CGRect(x: 0, y: 0, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
        teamsTableView.frame = CGRect(x: self.scrollView.frame.width, y: 0, width: self.scrollView.frame.width, height: self.scrollView.frame.height)
    }


    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == teamsTableView {
            return 2
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == newsTableView {
            return followedTeams?.count ?? 0
        } else {
            if section == 0 {
                return teams?.count ?? 0
            } else {
                return followedTeams?.count ?? 0
            }
        }
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == newsTableView {
            return 120
        } else {
            return 150
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == newsTableView {
            return 0
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == newsTableView {
            return UIView()
        } else {
            let header = UIView()
            header.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
            let textLabel = UILabel()
            textLabel.frame = CGRect(x: 15, y: 16, width: self.view.frame.width - 15, height: 20)
            textLabel.font = UIFont(name: "OpenSans", size: 11)
            textLabel.textColor = UIColor.black
            header.addSubview(textLabel)
            
            if section == 0 {
                textLabel.text = "TEAMS TO FOLLOW"
                return header
                
            } else {
                textLabel.text = "FOLLOWED TEAMS"
                return header
            }
            
        }
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView()
        footer.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        return footer
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TeamCell = tableView.dequeueReusableCell(withIdentifier: "TeamCell", for: indexPath) as! TeamCell
        if tableView == newsTableView {
            guard let team = followedTeams?[indexPath.row] else {return cell}
            cell.teamName.text = ("\(team.teamName ?? "") news")
            cell.teamLogo.image = UIImage(named: team.imageName!)
            cell.followButton.alpha = 0
            if team.numberOfNewPosts > 0 {
                cell.badgeLabel.alpha = 1.0
                cell.badge.alpha = 1.0
                cell.badgeLabel.text = "\(team.numberOfNewPosts)"
            } else {
                cell.badgeLabel.alpha = 0.0
                cell.badge.alpha = 0.0
            }
        } else {
            cell.selectionStyle = .none
            var team: NbaTeam?
            if indexPath.section == 0 {
                team = teams?[indexPath.row]
            } else {
                team = followedTeams?[indexPath.row]
            }
            if team == nil {
                return cell
            }
            cell.teamName.text = team?.teamName
            cell.teamLogo.image = UIImage(named: (team?.imageName)!)
            cell.followButton.setTitleColor(UIColor.blue, for: UIControlState.normal)
            if team?.followed.value ?? false {
                cell.followButton.setTitle("- unfollow", for: .normal)
                cell.followButton.followed = true
            } else {
                cell.followButton.setTitle("+ follow", for: .normal)
                cell.followButton.followed = false
            }
            cell.badge.alpha = 0
            cell.badgeLabel.alpha = 0
            cell.followButton.teamId = team?.id.value
            cell.followButton.removeTarget(self, action: #selector(followBttnPressed(sender:)), for: .touchUpInside)
            cell.followButton.addTarget(self, action: #selector(followBttnPressed(sender:)), for: .touchUpInside)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == teamsTableView {
            return
        } else {
            guard let team = followedTeams?[indexPath.row] else {return}
            selectedTeamId = team.id.value
            selectedTeamName = team.teamName
            performSegue(withIdentifier: "showNews", sender: self)
        }
    }
   @objc func followBttnPressed(sender: FollowButton) {
    
    let alert = UIAlertController(title: "Are you sure?", message: nil, preferredStyle: .actionSheet)
    let cancelAlert = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        return
    }
    let yesAlert = UIAlertAction(title: "Yes", style: .default) { action in
        sender.loadingIndicator(true)
        var follow: Bool?
        if sender.followed ?? false {
            follow = false
        } else {
            follow = true
        }
        let realm = try! Realm()
        guard let team =  realm.objects(NbaTeam.self).filter("id = \(sender.teamId ?? 0)").first else {
            sender.loadingIndicator(false)
            return
        }
        self.updateObject(object: team, followed: follow!, completion: { success in
            if success {
                if sender.followed == false  {
                    ParseNewsFeed(teamId: sender.teamId, completion: { success in
                        if success {

                            print("succesfully updated feeds")
                            self.teamsTableView.reloadData()
                        } else {
                            print("cant get feeds for this team")
                        }
                    })
                }
            }
         
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                sender.loadingIndicator(false)
                self.teamsTableView.reloadData()
                self.newsTableView.reloadData()
            }
        })

    }
    
    alert.addAction(cancelAlert)
    alert.addAction(yesAlert)
    
    self.present(alert, animated: true, completion: nil)
    
    }
    func updateObject(object: NbaTeam, followed: Bool,completion: @escaping (Bool) -> ()) {
        if let realm = object.realm {
            do {
                try realm.write({
                    object.followed.value = followed
                })
            } catch {
                completion(false)
                print(error)
            }
            completion(true)
        }
        completion(false)
    }

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == newsTableView || scrollView == teamsTableView {
            return
        }
        if shouldChangePage {
            let page = scrollView.contentOffset.x / self.view.bounds.width

            if page < 0.5 {
                self.newsLabel.alpha = 1.0
                self.teamsLabel.alpha = 0.7
                currentPage = 1
            }
                
            else if page >= 0.5 {
                self.newsLabel.alpha = 0.7
                self.teamsLabel.alpha = 1.0
                currentPage = 2
            }
            
            
            let contentOff = scrollView.contentOffset.x/2
            indicatorView.frame.origin.x = contentOff
        }
     

    }
   
    @objc func newsLabelTapped() {
        let scrollRect : CGRect = CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: self.scrollView.frame.size.height)
        currentPage = 1
        self.scrollView.scrollRectToVisible(scrollRect, animated: true)
    }
    @objc func teamsLabelTapped() {
        let scrollRect : CGRect = CGRect(x: self.view.frame.width, y: 0.0, width: self.view.bounds.size.width, height: self.scrollView.frame.size.height)
        currentPage = 2
        self.scrollView.scrollRectToVisible(scrollRect, animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination.isKind(of: StoryNewsVC.self) {
            let destinationVc = segue.destination as? StoryNewsVC
            destinationVc?.teamId = selectedTeamId
            destinationVc?.delegateTeams = self
            destinationVc?.navTitle = selectedTeamName
        }
    }
}
extension UIButton {
    func loadingIndicator(_ show: Bool) {
        let tag = 808404
        if show {
            self.isEnabled = false
            self.setTitle("", for: .normal)
            self.alpha = 1
            let indicator = UIActivityIndicatorView()
            indicator.color = UIColor.blue
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            self.isEnabled = true
            self.alpha = 1.0
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
}
