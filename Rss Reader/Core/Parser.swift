//
//  Parser.swift
//  Rss Reader
//
//  Created by Marko Piskac on 05/12/2017.
//  Copyright © 2017 Marko Piskac. All rights reserved.
//

import RealmSwift
import UIKit
import FeedKit

func ParseNbaTeams(jsondata: Any, completion: @escaping (Bool) -> ()) {
    DispatchQueue.global(qos: .userInitiated).async {
        var json : Any? = nil
        do {
            json = try JSONSerialization.jsonObject(with: jsondata as! Data, options: .allowFragments)
            let realm = try! Realm()
            if let jsondict = json as? Dictionary<String, Any> {
               
                if jsondict["teams"] != nil {
                    if let jsonarray = jsondict["teams"] as? [Any] {
                        try! realm.write() {
                            for djson in jsonarray as! [Dictionary<String, Any>] {
                                let nbaTeam = NbaTeam()
                                if djson["id"] != nil {
                                    nbaTeam.id.value = djson["id"] as? Int
                                }
                                if djson["followed"] != nil {
                                    nbaTeam.followed.value = djson["followed"] as? Bool
                                }
                                if djson["team_name"] != nil {
                                    nbaTeam.teamName = djson["team_name"] as? String
                                }
                                if djson["rss_link"] != nil {
                                    nbaTeam.rssLink = djson["rss_link"] as? String
                                }
                                if djson["imageName"] != nil {
                                    nbaTeam.imageName = djson["imageName"] as? String
                                }
                                realm.add(nbaTeam, update: true)
                            }
                        }
                    }
                }
            }
            DispatchQueue.main.async {
                completion(true)
            }
        } catch {
            DispatchQueue.main.async {
                completion(false)
            }
        }
    }
}

func ParseNewsFeed(teamId:Int?,completion: @escaping (Bool) -> ()) {
    
    DispatchQueue.global(qos: .userInitiated).async {
        do {
            var nbaTeams: Results<NbaTeam>? = nil
            let realm = try! Realm ()
            if teamId != nil {
                nbaTeams = realm.objects(NbaTeam.self).filter("id = \(teamId ?? 0)")
            } else {
               nbaTeams = realm.objects(NbaTeam.self)
            }
            if nbaTeams == nil {
                DispatchQueue.main.async {
                    completion(true)
                }
            }
            for team in nbaTeams! {
                if team.followed.value == true {
                    let keyList: [String] = team.newsFeed.map{$0.linkUrl ?? ""}
                    let feedUrl = URL(string: team.rssLink!)
                    let parser = FeedParser(URL: feedUrl!)
                    if parser != nil {
                        let result = parser?.parse()
                            try! realm.write() {
                                if let feedItems = result?.rssFeed?.items, (result?.isSuccess)!  {
                                    //team.newsFeed.removeAll()
                                    for item in feedItems.reversed() {
                                        guard let link = item.link else {continue}
                                        if keyList.contains(link){
                                            continue
                                        }
                                        
                                        let newFeed = NewsFeed()
                                        newFeed.linkUrl = item.link
                                        if item.title != nil {
                                            newFeed.title = item.title
                                        }
                                        if item.description != nil {
                                            newFeed.feedDescription = item.description
                                        }
                                        if item.pubDate != nil {
                                            newFeed.pubDate = item.pubDate
                                        }
                                        
                                        if item.enclosure?.attributes?.url != nil {
                                            newFeed.imageUrl = item.enclosure?.attributes?.url
                                        }
                                        
                                        team.newsFeed.append(newFeed)
                                        team.numberOfNewPosts = team.numberOfNewPosts + 1
                                    }
                                    realm.add(team,update: true)
                                }
                        }
                        
                    } else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        print("No data for team \(team.teamName ?? "")")
                    }
                }
            }
            DispatchQueue.main.async {
                completion(true)
            }
        }
    }
}
func AddNewUserFeed(rssLink:String,title:String, completion: @escaping (Bool) -> ()) {
    DispatchQueue.global(qos: .userInitiated).async {
        do {
            let feedUrl = URL(string: rssLink)
            let parser = FeedParser(URL: feedUrl!)
            if parser != nil {
                    let result = parser?.parse()
                    let realm = try! Realm()
                    try! realm.write() {
                        let feed = UserFeed()
                        feed.feedTitle = title
                        feed.rssLink = rssLink
                        feed.numberOfNewPosts = 0
                        feed.imageUrl = result?.rssFeed?.image?.url
                        feed.id = rssLink
                        realm.add(feed,update:true)
                            
                }
            } else {
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        }
        DispatchQueue.main.async {
            completion(true)
        }
    }
}

func RemoveUserFeed(rssLink:String, completion: @escaping (Bool) -> ()) {
    let realm = try! Realm()
    let object = realm.object(ofType: UserFeed.self, forPrimaryKey: rssLink)
    if object == nil {
        DispatchQueue.main.async {
            completion(false)
        }
    }
    try! realm.write() {
        realm.delete(object!)
    }
    DispatchQueue.main.async {
        completion(true)
    }
}

func ParseUsersFeed(feedId:String?,completion: @escaping (Bool) -> ()) {
    
    DispatchQueue.global(qos: .userInitiated).async {
        do {
            var feeds: Results<UserFeed>? = nil
            let realm = try! Realm ()
            if feedId == nil {
                feeds = realm.objects(UserFeed.self)
            } else {
                let qStr = ""
                feeds = realm.objects(UserFeed.self).filter("id = '\(feedId ?? qStr)'")
            }
          
            if feeds == nil {
                DispatchQueue.main.async {
                    completion(false)
                }
            }
            for feed in feeds! {
                    let keyList: [String] = feed.newsFeed.map{$0.linkUrl ?? ""}
                    let feedUrl = URL(string: feed.rssLink!)
                    let parser = FeedParser(URL: feedUrl!)
                    if parser != nil {
                        let result = parser?.parse()
                        try! realm.write() {
                            if let feedItems = result?.rssFeed?.items, (result?.isSuccess)!  {
                                for item in feedItems.reversed() {
                                    guard let link = item.link else {continue}
                                    if keyList.contains(link){
                                        continue
                                    }
                                    let newFeed = NewsFeed()
                                    newFeed.linkUrl = item.link
                                    if item.title != nil {
                                        newFeed.title = item.title
                                    }
                                    if item.description != nil {
                                        newFeed.feedDescription = item.description
                                    }
                                    if item.pubDate != nil {
                                        newFeed.pubDate = item.pubDate
                                    } else {
                                        newFeed.pubDate = Date()
                                    }
                                    
                                    if item.enclosure?.attributes?.url != nil {
                                        newFeed.imageUrl = item.enclosure?.attributes?.url
                                    }
                                    feed.newsFeed.append(newFeed)
                                    feed.numberOfNewPosts = feed.numberOfNewPosts + 1
                                }
                                realm.add(feed,update: true)
                            }else {
                                DispatchQueue.main.async {
                                    completion(false)
                                }
                            }
                           
                        }
                        
                    } else {
                        DispatchQueue.main.async {
                            completion(false)
                        }
                        print("No data for team ")
                    }
                
            }
            
        }
        DispatchQueue.main.async {
            completion(true)
        }
    }
}
func resetNotificationsNUmber(teamId:Int?,storyId: String?,completion: @escaping (Bool) -> ()) {
    
    DispatchQueue.global(qos: .userInitiated).async {
        do {
            
            let realm = try! Realm()
            var teamObject: NbaTeam?
            var userObject: UserFeed?
            if teamId != nil {
                teamObject =  realm.objects(NbaTeam.self).filter("id = \(teamId ?? 0)").first
            } else {
                let qStr = ""
                userObject =  realm.objects(UserFeed.self).filter("id = '\(storyId ?? qStr)'").first
            }
            if userObject == nil && teamObject == nil {
                DispatchQueue.main.async {
                    completion(false)
                }
            }
            try! realm.write {
                if teamId == nil {
                    userObject?.numberOfNewPosts = 0
                } else {
                    teamObject?.numberOfNewPosts = 0
                }
            }
        }
        DispatchQueue.main.async {
            completion(true)
        }
    }
}
//func RemoveStorysForFeed(teamId:Int?,storyId: String?,completion: @escaping (Bool) -> ()) {
//    
//    DispatchQueue.global(qos: .userInitiated).async {
//        do {
//            
//            let realm = try! Realm()
//            var teamObject: NbaTeam?
//            var userObject: UserFeed?
//            if teamId != nil {
//                teamObject =  realm.objects(NbaTeam.self).filter("id = \(teamId ?? 0)").first
//            } else {
//                let qStr = ""
//                userObject =  realm.objects(UserFeed.self).filter("id = '\(storyId ?? qStr)'").first
//            }
//            if userObject == nil && teamObject == nil {
//                DispatchQueue.main.async {
//                    completion(false)
//                }
//            }
//            try! realm.write {
//                if teamId == nil {
//                    userObject?.newsFeed.removeAll()
//                } else {
//                    teamObject?.newsFeed.removeAll()
//                }
//            }
//        }
//        DispatchQueue.main.async {
//            completion(true)
//        }
//    }
//}

