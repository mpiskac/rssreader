//
//  TeamNewsCell.swift
//  Rss Reader
//
//  Created by Marko Piskac on 05/12/2017.
//  Copyright © 2017 Marko Piskac. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    @IBOutlet weak var feedImage: UIImageView!
    @IBOutlet weak var pubDateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
