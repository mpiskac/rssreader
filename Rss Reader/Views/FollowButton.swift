//
//  FollowButton.swift
//  Rss Reader
//
//  Created by Marko Piskac on 05/12/2017.
//  Copyright © 2017 Marko Piskac. All rights reserved.
//

import UIKit

class FollowButton: UIButton {
    var teamId: Int?
    var followed: Bool?
}
