//
//  TeamCell.swift
//  Rss Reader
//
//  Created by Marko Piskac on 04/12/2017.
//  Copyright © 2017 Marko Piskac. All rights reserved.
//

import UIKit

class TeamCell: UITableViewCell {

    var teamName: UILabel = UILabel()
    var teamLogo: UIImageView = UIImageView()
    var followButton: FollowButton = FollowButton()
    var badge: UIView = UIView()
    var badgeLabel: UILabel = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.preservesSuperviewLayoutMargins = false
        self.separatorInset = UIEdgeInsets.zero
        self.layoutMargins = UIEdgeInsets.zero
        
        teamLogo.translatesAutoresizingMaskIntoConstraints = false
        teamName.translatesAutoresizingMaskIntoConstraints = false
        followButton.translatesAutoresizingMaskIntoConstraints = false
        badge.translatesAutoresizingMaskIntoConstraints = false
        badgeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        teamName.font = UIFont(name: "Futura-Medium", size: 18)
        teamName.textColor = UIColor.darkGray
        
        followButton.titleLabel?.font = UIFont(name: "Futura-MediumItalic", size: 20)
        let bttnColor = UIColor(red: 0/255, green: 128/255, blue: 255/255, alpha: 1.0)
        followButton.setTitleColor(bttnColor, for: .normal)
        
        teamName.adjustsFontSizeToFitWidth = true
        teamName.sizeToFit()
        
        badge.layer.cornerRadius = 10.0
        badge.backgroundColor = .red
        
        badgeLabel.textColor = .white
        badgeLabel.textAlignment = .center
        badgeLabel.font = UIFont.boldSystemFont(ofSize: 10.0)
       
        self.contentView.addSubview(teamName)
        self.contentView.addSubview(teamLogo)
        self.contentView.addSubview(followButton)
        self.contentView.addSubview(badge)
        self.badge.addSubview(badgeLabel)
        
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        teamLogo.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        teamLogo.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 25).isActive = true
        teamLogo.heightAnchor.constraint(equalToConstant: self.frame.height * 0.5).isActive = true
        teamLogo.widthAnchor.constraint(equalToConstant: self.frame.height * 0.5).isActive = true
        
        teamName.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        teamName.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 10).isActive = true
        teamName.leadingAnchor.constraint(equalTo: teamLogo.trailingAnchor,constant: 25).isActive = true
        teamName.heightAnchor.constraint(equalToConstant: self.frame.height * 0.5).isActive = true
       
        followButton.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        followButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        followButton.widthAnchor.constraint(equalToConstant: 120).isActive = true
        followButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        badge.topAnchor.constraint(equalTo: self.topAnchor, constant: 15).isActive = true
        badge.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20).isActive = true
        badge.widthAnchor.constraint(equalToConstant: 20).isActive = true
        badge.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        badgeLabel.centerXAnchor.constraint(equalTo: badge.centerXAnchor).isActive = true
        badgeLabel.centerYAnchor.constraint(equalTo: badge.centerYAnchor).isActive = true
        badgeLabel.widthAnchor.constraint(equalToConstant: 15).isActive = true
        badgeLabel.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
    }

}
